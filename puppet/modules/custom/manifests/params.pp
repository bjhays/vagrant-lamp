class custom::params {
    $host = 'local.devbox.com'
    $docroot = '/var/www/project/'
    $port = '80'

    $import_mysql_file_location = "/tmp/vagrant-puppet/sql/db2import.sql"
    $filepath = '/puppet/modules'
	$serveraliases = ''

    # Db settings
    $mysql_password = '1@mR00t'
    $dbname = 'project'
    $dbuser = 'project'
    $dbpass = 'secret'
    $phpmyadmin_conf = "/etc/httpd/conf.d/phpMyAdmin.conf"
    $config_inc_php = "/etc/phpMyAdmin/config.inc.php"

    case $::operatingsystem {
	    debian, ubuntu: {
	      $ok = true
	      $apache = ['apache2', 'apache2.2-common']
	      $apache_srv = 'apache2'
	      $apache_conf = '/etc/apache2/apache2.conf'
	      $php = ['php5', 'php5-mysql', 'libapache2-mod-php5']
	      $mysql = ['mysql-server', 'libapache2-mod-auth-mysql']
	      $mysql_srv = 'mysql'
	      $mysql_conf = '/etc/mysql/my.cnf'
	      $mysql_dev = ['mysql-devel']
	      $userdir_cmd = '/usr/sbin/a2enmod userdir'
	      $mysqlpw_cmd = '/usr/bin/mysqladmin -u root password $mysql_password || /bin/true'
	      $sysconfig_conf = '/tmp/hng'
	      $init_cmd = '/sbin/initctl reload-configuration'
	      $www = '/var/www/index.php'
	      $phpmyadmin = ['phpMyAdmin']
	    }
	    centos, redhat, oel, linux: {
	      $ok = true
	      $apache = ['httpd', 'httpd-devel']
	      $apache_srv = 'httpd'
	      $apache_conf = '/etc/httpd/conf/httpd.conf'
	      $php = ['php', 'php-mysql', 'php-common', 'php-gd', 'php-mbstring', 'php-devel', 'php-xml', 'php-mcrypt']
	      $mysql = ['mysql', 'mysql-devel'] # 'mysql-server',
	      $mysql_dev = ['mysql-devel']
	      $mysql_srv = 'mysqld'
	      $mysql_conf = '/etc/my.cnf'
	      $userdir_cmd = '/bin/true'
	      $mysqlpw_cmd = 'echo "UPDATE mysql.user SET Password=PASSWORD(\'$mysql_password\') WHERE User=\'root\';FLUSH PRIVILEGES;" > mysql'
	      $init_cmd = '/bin/true'
	      $sysconfig_conf = '/etc/sysconfig/httpd'
	      $www = '/var/www/html/index.php'
	      $phpmyadmin = ['mysql-devel','phpMyAdmin']
	    }
	    default: {
	      fail("This module is not supported on ${operatingsystem}")
	    }
	}
}