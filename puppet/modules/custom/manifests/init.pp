class custom {
    include custom::params
    
    if ($custom::params::ok) {
      notify{"Valid OS": }
      Package { ensure => $custom::params::version, }

      File { owner => 'root', group => 'root', mode => '0644', }

      notify{"Install $custom::params::apache": }
      package { $custom::params::apache: }

      service { $custom::params::apache_srv:
        ensure  => running,
        enable  => true,
        require => Package[$custom::params::apache],
      }


      file { $custom::params::apache_conf:
        ensure  => present,
        source  => "puppet:///modules/custom/apache2/apache2.${operatingsystem}.conf",
        require => Package[$custom::params::apache],
        notify  => Service[$custom::params::apache_srv],
      }

      file { $custom::params::sysconfig_conf:
        ensure  => present,
        source  => "puppet:///modules/custom/sysconfig/httpd.${operatingsystem}",
        require => Package[$custom::params::apache],
        notify  => Service[$custom::params::apache_srv],
      }

      exec { 'userdir':
        notify  => Service[$custom::params::apache_srv],
        command => $custom::params::userdir_cmd,
        require => Package[$custom::params::apache],
      }

      #install php
      package { $custom::params::php: require => Package[$custom::params::apache], }

      # Install mysql
      class { '::mysql::server':
        root_password    => $custom::params::mysql_password,
        override_options => { 'mysqld' => { 'max_connections' => '1024' } }
      }

      # create db, user and give access from outside of VM
      mysql::db { $custom::params::dbname:
        user     => $custom::params::dbuser,
        password => $custom::params::pass,
        host     => '192.168.1.%',
      }
      # localhost entry for user
      mysql_grant { "${custom::params::dbuser}/localhost":
        ensure     => 'present',
        options    => ['GRANT'],
        privileges => ['ALL'],
        table      => "$custom::params::dbname.*",
        user       => "${custom::params::dbuser}@localhost",
      }

      # install phpmyadmin with access from the local VM subnet in config mode
      class { 'phpmyadmin':
          ip_access_ranges => ["192.168."],
          root_password => $custom::params::mysql_password,
          auth_type => "config"
      }
      
      #open ports
      include iptables

      #import sql
      $a = file('/etc/puppet/modules/local/files/bash_profile.local','/dev/null')
      if($a != '') {
          #2do make this dynamic loading all files in dir
          #$custom::params::import_mysql_file_location
          notify{"Importing DB": }
          exec { 'import_db':
            command => "mysql -u${custom::params::dbuser} -p${custom::params::pass} > ${custom::params::import_mysql_file_location}",
            require => Package['mysql-server'],
          }
      }


    }
  else{
    notify{"invalid OS": }  
  }
}
